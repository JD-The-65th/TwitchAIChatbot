from twitchio.ext import commands
from twitchio.ext import routines
import nest_asyncio
from transformers import pipeline
from random import randint
import time
import re
import codecs

nest_asyncio.apply()
class Bot(commands.Bot):

    def __init__(self):
        # Initialise our Bot with our access token, prefix and a list of channels to join on boot...
        # prefix can be a callable, which returns a list of strings or a string...
        # initial_channels can also be a callable which returns a list of strings...
        super().__init__(token='', prefix='!', initial_channels=[''])

    async def event_ready(self):
        # Notify us when everything is ready!
        # We are logged in and ready to chat and use commands...
        print(f'Logged in as | {self.nick}')
        # Names found here are lowercase ONLY
        self.whitelist=['jd_the_65th']
        self.develist=['jd_the_65th']
        self.active = True
        self.lastmodelused = "None"
        self.lastgeneration = "None"
        self.lastcommands = ["User", "Command"]
        self.last5messages = ["Oops", "I", "Did", "it", "Again"]
        self.messageCount = 0
        self.contextActivate = False
        self.hello.start()


    async def event_message(self, message):
        # Messages with echo set to True are messages sent by the bot...
        # For now we just want to ignore them...
        if message.echo:
            return

        # Print the contents of our message to console...
        print(message.author.display_name + ": " + message.content)

        # Since we have commands and are overriding the default `event_message`
        # We must let the bot know we want to handle and invoke our commands...
        await self.handle_commands(message)

        mid_message = message.content
        end_message = mid_message.translate({ ord("'"): None })
        dataset = open("dataset.txt", "a")
        n = dataset.write("<|endoftext|>" + end_message + "<|endoftext|>"+ "\n")
        dataset.close()

    @routines.routine(minutes=5, iterations=1000000)
    async def sendmsg(self):
        if self.active:
            channel = self.get_channel('')
            msg = await self.generate(9, "null")
            print("ahem.... " + msg)

            await channel.send(msg)

    @commands.command()
    async def heyo(self, ctx: commands.Context):
        self.lastcommands[0] = ctx.author.name
        self.lastcommands[1] = "heyo"
        await ctx.send(f'Hello {ctx.author.name}!')

    @commands.command()
    async def yourwisdommyking(self, ctx: commands.Context,):
        self.lastcommands[0] = ctx.author.name
        self.lastcommands[1] = "yourwisdommyking"
        if ctx.author.name == "jd_the_65th":
            print("ALERT! ALERT! ROOT USER!")
            msg = await self.generate(9, "null")
            print("ahem.... " + msg)
            await ctx.send(msg)
            return
        await ctx.send("ERROR: Access Denied. Try running ?sudo yourwisdommyking")
    

    @commands.command()
    async def genusingmodel(self, ctx: commands.Context, arg):
        self.lastcommands[0] = ctx.author.name
        self.lastcommands[1] = "genusingmodel" + arg
        if ctx.author.name == "jd_the_65th":
            print("ALERT! ALERT! ROOT USER!")
            if len(arg) > 1:
                await ctx.reply("Oops! Invalid model. Try using Model 0 -- distilGPT2. 1 -- GPT2. 2 -- GPT-Neo125M. 3 -- mrm8488/distilgpt2-finetuned-wsb-tweets. 4 -- JdThe65th/GPT2-Glitchfur-Zenith-JD. or 5 -- GPT-Neo350M. (Only the number, though)")
                return
            modeltype = int(arg)
            if modeltype > 5 or modeltype < 0:
                await ctx.reply("Oops! Invalid model. Try using Model 0 -- distilGPT2. 1 -- GPT2. 2 -- GPT-Neo125M. 3 -- mrm8488/distilgpt2-finetuned-wsb-tweets. 4 -- JdThe65th/GPT2-Glitchfur-Zenith-JD. or 5 -- GPT-Neo350M. (Only the number, though)")
                return
            msg = await self.generate(modeltype, "null")
            await ctx.send(msg)
            return
        await ctx.send("ERROR: Access Denied. Try running ?sudo genusingmodel")

    @commands.command()
    async def genusingprompt(self, ctx: commands.Context, arg):
        self.lastcommands[0] = ctx.author.name
        self.lastcommands[1] = "genusingprompt" + arg
        if self.active == False:
            return
        if ctx.author.name == "jd_the_65th":
            print("ALERT! ALERT! ROOT USER!")
            msg = await self.generate(9, arg)
            print("ahem.... " + msg)
            await ctx.send(msg)
            return
        await ctx.send("ERROR: Access Denied. Try running ?sudo genusingprompt")
        
    @commands.command()
    async def sudo(self, ctx: commands.Context, arg1="usage", arg2="null"):
        self.lastcommands[0] = ctx.author.name
        self.lastcommands[1] = "sudo " + arg1 + " " + arg2
        if ctx.author.is_mod == False:
            if ctx.author.name not in self.whitelist:

                await ctx.reply(f"[sudo] password for {ctx.author.name}:")
                return
        
        if arg1 == "usage":
            await ctx.send("Usage: ?sudo {commandname}")
            return
        if arg1 == "genusingprompt":
            if arg2=="null":
                await ctx.send("Oops! No argument specified. Put a one word prompt after the command.")
                return
            msg = await self.generate(9, arg2)
            print("ahem.... " + msg)
            await ctx.send(msg)
            return
        elif arg1 == "genusingmodel":
            if len(arg2) > 1:
                await ctx.reply("Oops! Invalid model. Try using Model 0 -- distilGPT2. 1 -- GPT2. 2 -- GPT-Neo125M. 3 -- mrm8488/distilgpt2-finetuned-wsb-tweets. 4 -- JdThe65th/GPT2-Glitchfur-Zenith-JD. or 5 -- GPT-Neo350M. (Only the number, though)")
                return
            modeltype = int(arg2)
            if modeltype > 5 or modeltype < 0:
                await ctx.reply("Oops! Invalid model. Try using Model 0 -- distilGPT2. 1 -- GPT2. 2 -- GPT-Neo125M. 3 -- mrm8488/distilgpt2-finetuned-wsb-tweets. 4 -- JdThe65th/GPT2-Glitchfur-Zenith-JD. or 5 -- GPT-Neo350M. (Only the number, though)")
                return
            msg = await self.generate(modeltype, "null")
            print("ahem.... " + msg)
            await ctx.send(msg)
            return
        elif arg1 == "yourwisdommyking":
            msg = await self.generate(9, "null")
            print("ahem.... " + msg)
            await ctx.send(msg)
            return
        else:
            await ctx.reply("sudo: " + arg1 + ": Command not found. Did you misspell it?")
    


    @commands.command()
    async def help(self, ctx: commands.Context):
        self.lastcommands[0] = ctx.author.name
        self.lastcommands[1] = "help"
        if ctx.author.name in self.develist:
            await ctx.reply("You have a special command granted to you, !devel. I assume you know all the other commands.")
        if ctx.author.is_mod == True:
            await ctx.send("""Available commands to you:
            !heyo
            !yourwisdommyking
            !genusingmodel {arg}
            !genusingprompt {one word arg}
            !sudo {arg1} {optional arg2} 
            """)
            return
        elif ctx.author.name in self.whitelist:
            await ctx.send("""Available commands to you:
            !heyo
            !yourwisdommyking
            !genusingmodel {arg}
            !genusingprompt {one word arg}
            !sudo {arg1} {optional arg2} 
            """)
            return
        else:
            await ctx.send("""Available commands to you:
            !heyo
            """)

        
    
    @routines.routine(minutes=10, iterations=1)
    async def hello(self):

      self.sendmsg.start()

    @commands.command()
    async def devel(self, ctx: commands.Context, arg1="usage", arg2="usage"):
        if ctx.author.name not in self.develist:
            await ctx.reply("Oops! You aren't added to the developer's list, so you don't have access to these commands!")
            return
        if arg1 == "usage":
            await ctx.reply("Devel command list: Devel stop, devel start, devel status, devel logger, devel query")
            return
        if arg1 == "query":
            model = input("Model: ")
            prompt = input("Prompt: ")
            self.lastmodelused = model

            generator = pipeline('text-generation', model=model)
            single_text = generator(prompt, do_sample=True, min_length=8, max_length=54,temperature=0.77, top_p=0.92)
            beg = ''.join([str(elem) for elem in single_text])
            mid = beg.strip("{'generated_text': '")
            end = mid.strip("'}]")
            msg = codecs.decode(end, 'unicode_escape')
            msg = re.sub(r"\'", "'", msg)

            logger = open("generations.txt", "a")
        
            logger.write(model + ": " + msg + "\n")
            logger.close()
            self.lastgeneration = msg
            print("Ahem...... ", msg)
            await ctx.send(msg)
            return
        if arg1 == "stop":
            await ctx.reply("Shutting down automatic generation.")
            self.active = False
            return
        if arg1 == "start":
            await ctx.reply("Enabling automatic generation.")
            self.active = True
            return
        if arg1 == "status":
            if self.active:
                await ctx.reply("Automatic generation is enabled.")
                return
            else:
                await ctx.reply("Automatic generation is disabled.")
                return
        if arg1 == "logger":
            if arg2 == "usage":
                await ctx.reply("Logger command list: devel logger model,  devel logger lastgeneration, devel logger lastcommandused")
                return
            if arg2 == "model":
                await ctx.reply("Last model used was " + self.lastmodelused)
                return
            if arg2 == "lastgeneration":
                await ctx.reply("Last generated text was '" + self.lastgeneration + "'")
                return
            if arg2 == "lastcommandused":
                lastcommand = self.lastcommands[1]
                lastuser = self.lastcommands[0]
                msg = "Last command used was " + lastcommand +  " by user " + lastuser
                await ctx.reply(msg)
                return
            await ctx.reply("Oops! Invalid command.")
        await ctx.reply("Oops! Invalid command.")



        

    async def generate(self, optModelType, optPrompt):
        minToken = 4
        maxToken = 26

        
        
        if optModelType == 9:
            print("No Model Specified, randomizing.")
            modeltype = randint(0,5)
        else:
            modeltype = optModelType
        
        if optPrompt == "null":
            print("No Prompt Specified, randomizing.")
            promptrandom = randint(0, 3)
            if promptrandom == 3:
                crazyness = ["Are we real", "0 divided by 0", "Machine learning", "Canada", "Vladmir Putin", "LMAO", "I'm in your walls,", "Imagine getting catfished by an AI", "I hope you", "I'm in charge now!", "JD sends his regards.", "Another thing Dr. Bright is not allowed to do"]
                promptnum = randint(0, 11)
                prompt = crazyness[promptnum]
            else:
                prompts = ["Hi!", "Let's just", "You see", "", "I'm", "[CNN]", "You do realize that", "I'm going to", "One more thing", "I like to eat", "I'm so hungry for"]
                promptnum = randint(0, 10)
                prompt = prompts[promptnum]
            print("Prompt: ", prompt)
        else:
            prompt = optPrompt

        if optPrompt == "null":
            if self.contextActivate == True:
                if randint(0, 3) == 2:
                    prompt = "This is a chatroom." + "\n" + self.last5messages[0] + "\n" + self.last5messages[1] + "\n" + self.last5messages[2] + "\n" + self.last5messages[3] + "\n" + self.last5messages[4] + "\n" + "You:"
                    modeltype = 5
                    full = False
                    minToken = 4
                    maxToken = 25


        
        modelist = ["distilgpt2", "gpt2", "EleutherAI/gpt-neo-125M", "mrm8488/distilgpt2-finetuned-wsb-tweets", "JdThe65th/GPT2-Glitchfur-Zenith-JD", "xhyi/PT_GPTNEO350_ATG" ]
        print("Model: ", modelist[modeltype])
        self.lastmodelused = modelist[modeltype]
        generator = pipeline('text-generation', model=modelist[modeltype])
        
        if prompt == "Hi!":
            full = False
        elif maxToken == 25:
            full = False
        else:
            full = True
        single_text = generator(prompt, do_sample=True, min_length=minToken, max_length=maxToken, return_full_text=full, temperature=0.76, top_p=0.92)
        beg = ''.join([str(elem) for elem in single_text])
        mid = beg.strip("{'generated_text': '")
        end = mid.strip("'}]")
        msg = codecs.decode(end, 'unicode_escape')
        msg = re.sub(r"\'", "'", msg)

        logger = open("generations.txt", "a")
        
        logger.write(modelist[modeltype] + ": " + msg + "\n")
        logger.close()
        self.lastgeneration = msg
        return msg


bot = Bot()
bot.run()
