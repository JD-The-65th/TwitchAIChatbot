# TwitchAIChatbot

An A.I. Chatbot using Huggingface Transformers written in python.

## Getting started

**This project was created and tested in [Conda.](https://www.anaconda.com/) Functionality outside of Conda may vary**

Run 

`pip install -r requirements.txt` 

To get the basic requirements. Tensorflow and Pytorch are **NOT** on this list, just in case you need to install the CPU, CUDA, or ROCm versions of them. 

You will also need to generate a token for your bot account [using this link](https://twitchtokengenerator.com/)



